import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TrafficIncidentReportServlet {

	private EditorService englishEditorService;
	private EditorService spanishEditorService;

	public TrafficIncidentReportServlet() {
		
	}
	
	public TrafficIncidentReportServlet(EditorService editorService) {
		this.englishEditorService = editorService;
	}
	
	@ResponseBody
    @RequestMapping(value = "/trafficincidentreports")
    public String run(@RequestParam(required = false, value = "issue_reported") String issue)
    {
		if (issue == null) {
			return "Welcome to Traffic Incident Report statistics calculation page. Please provide issue_reported as query parameter.";
		} else {
			int x = englishEditorService.XMLReader(issue);
			String severity = englishEditorService.riskLevel(x);
			return "Number of " + issue + " incidents: " + x + ". Risk level: " + severity + ".";
		}
    }
}
