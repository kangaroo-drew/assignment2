
public interface EditorService {
	
	public String composeEmail();
	
	public String getName();
	
	public int XMLReader(String key);
	
	public String riskLevel(int x);
}