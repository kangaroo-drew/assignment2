import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class EnglishEditorServiceImpl implements EditorService {

	String name = "English editor";
	
	public String composeEmail() {
		return "Hello world.";
	}

	public String getName() {
		return this.name;
	}
	
	public int XMLReader(String value) {
		int cntr = 0;
		String fileName = "http://www.cs.utexas.edu/~devdatta/traffic_incident_data.xml";
		
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
		    builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
		    e.printStackTrace();  
		}
		
		Document document = null;
		try {
			document = builder.parse((new URL(fileName).openStream()));
		} catch (SAXException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		Element rootElement = document.getDocumentElement();
		Queue<Element> q = new LinkedList<Element>();
		q.add(rootElement);
		
		while(!q.isEmpty()) {
			Element e = (Element) q.remove();		
			if (e.getNodeName().equalsIgnoreCase("ds:issue_reported")) {
		    	String nodeValue = e.getTextContent();
		    	if (nodeValue.equalsIgnoreCase(value)) {
		    		cntr++;
		    	}
		    }
			NodeList nodes = e.getChildNodes();
			for(int i=0; i<nodes.getLength(); i++) {
			  Node node = nodes.item(i);
			  if(node instanceof Element) {
				  q.add((Element) node);
			    }
			  }
			}
		return cntr;
	}
	
	public String riskLevel(int x) {
		String ans = null;
		
		if (0 <= x && x <= 5)
			ans = "Rare";
		else if (6 <= x && x <= 10)
			ans = "Very Low";
		else if (11 <= x && x <= 15)
			ans = "Low";
		else if (16 <= x && x <= 30)
			ans = "Medium";
		else if (31 <= x && x <= 50)
			ans = "Moderate";
		else if (51 <= x && x <= 100)
			ans = "High";
		else if (101 <= x && x <= 200)
			ans = "Very High";
		else if (201 <= x)
			ans = "Extreme";
		return ans;
	}
}
