import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

public class TestCalculatorServiceImpl {
	
	EnglishEditorServiceImpl engServiceImpl;
	
	@Before
	public void setup() {
		engServiceImpl = new EnglishEditorServiceImpl();
	}
	
	@Test
	public void testBadParam() {
		String ret = engServiceImpl.riskLevel(200);
		assertEquals("Very High", ret);
	}
	
	@Test
	public void testGoodParam() {
		String ret = engServiceImpl.riskLevel(201);
		assertEquals("Extreme", ret);
	}
	
	@Test
	public void testExactString() {
		String ret = engServiceImpl.riskLevel(6);
		assertEquals("Very Low", ret);
	}
}
