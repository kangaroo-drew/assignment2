import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TestEmailController {
	
	TrafficIncidentReportServlet emailController = new TrafficIncidentReportServlet();
	EditorService mockEditor = null;
	
	@Before
	public void setUp1() {		
		mockEditor = mock(EditorService.class);		
		emailController = new TrafficIncidentReportServlet(mockEditor);
	}
	
	@Test
	public void testHelloWorld() {
		when(mockEditor.XMLReader("asdf")).thenReturn(0);
		when(mockEditor.riskLevel(0)).thenReturn("Rare");
		String reply = emailController.run("asdf");
		assertEquals("Number of asdf incidents: 0. Risk level: Rare.", reply);
		verify(mockEditor).riskLevel(0);
	}
	
	@Test
	public void testGetComposedEmail() {
		when(mockEditor.XMLReader("CoLlision")).thenReturn(35);
		when(mockEditor.riskLevel(35)).thenReturn("Moderate");
		String reply = emailController.run("CoLlision");
		assertEquals("Number of CoLlision incidents: 35. Risk level: Moderate.", reply);
		verify(mockEditor).riskLevel(35);
	}
}
